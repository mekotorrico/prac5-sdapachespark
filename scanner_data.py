from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.types import *
from datetime import datetime
import os, argparse, sys

os.system('clear')
parser = argparse.ArgumentParser()
parser.add_argument("-s","--schema", help="Mostrar el esquema de datos", action="store_true")
parser.add_argument("-t","--table", help="Mostrar los primeros 30 y la cantidad de datos", action="store_true")
parser.add_argument("-dcs","--distinctCategorySKU", help="Mostrar las categorías distintas del SKU", action="store_true")
parser.add_argument("-gs","--groupSKU", help="Mostrar por SKU el importe total de ventas", action="store_true")
parser.add_argument("-fdb","--filterDateBetween", help="Mostrar por fecha el importe total de ventas entre 'YYYY-MM-DD,YYYY-MM-DD'")
args = parser.parse_args()

if len(sys.argv[1:]) == 0:
    print("No se introdujo ningún parametro.")
    sys.exit()

conf = SparkConf() \
    .setMaster("spark://spark-master:7077") \
    .setAppName("Scanner_Data") \
    .set("spark.driver.extraJavaOptions", "-Dio.netty.tryReflectionSetAccessible=true") \
    .set("spark.executor.extraJavaOptions", "-Dio.netty.tryReflectionSetAccessible=true")

sc = SparkSession \
    .builder \
    .config(conf=conf) \
    .getOrCreate()

csv = sc.sparkContext.textFile("scanner_data.csv")
header = csv.first()
data = csv.filter(lambda line: line != header) \
    .map(lambda line: line.split(',')) \
    .filter(lambda line: len(line)>1) \
    .map(lambda line: (datetime.strptime(line[1],"%Y-%m-%d"), int(line[2]), int(line[3]), line[4], line[5], float(line[6]), float(line[7]) )) \
    .collect()

rdd = sc.sparkContext.parallelize(data)

schema = StructType([ \
        StructField("Date", DateType(),True), \
        StructField("Customer_ID", IntegerType(),True), \
        StructField("Transaction_ID", IntegerType(),True), \
        StructField("SKU_Category", StringType(), True), \
        StructField("SKU", StringType(), True), \
        StructField("Quantity", DoubleType(), True), \
        StructField("Sales_Amount", DoubleType(), True) \
    ])

df = sc.createDataFrame(rdd, schema)

if args.schema:
    os.system('clear')
    df.printSchema()
    print('')
if args.table:
    count = df.count()
    os.system('clear')
    df.show(30, False)
    print("\nTotal de datos: "+str(count)+"\n")
if args.distinctCategorySKU:
    count = df.select('SKU_Category') \
        .distinct() \
        .count()
    os.system('clear')
    df.select('SKU_Category') \
        .distinct() \
        .show(count, False)
    print("\nTotal de categorías distintas del SKU: "+str(count)+"\n")
if args.groupSKU:
    count = df.groupBy('SKU') \
        .sum('Sales_Amount') \
        .count()
    os.system('clear')
    df.groupBy('SKU') \
        .sum('Sales_Amount') \
        .show(count, False)
    print("\nTotal de resultados: "+str(count)+"\n")
if args.filterDateBetween is not None:
    try:
        dates = args.filterDateBetween.split(',')
        dates[0] = datetime.strptime(dates[0],"%Y-%m-%d")
        dates[1] = datetime.strptime(dates[1],"%Y-%m-%d")
        dates.sort()
    except:
        print("Parametro invalido.")
        sys.exit()
    count = df.filter("Date >= '" + str(dates[0]) + "' and Date < '" + str(dates[1]) + "'") \
        .groupBy("Date") \
        .sum('Sales_Amount') \
        .count()
    os.system('clear')
    df.filter("Date >= '" + str(dates[0]) + "' and Date < '" + str(dates[1]) + "'") \
        .groupBy("Date") \
        .sum('Sales_Amount') \
        .orderBy("Date") \
        .show(count, False)
    print("\nTotal de resultados: "+str(count)+"\n")